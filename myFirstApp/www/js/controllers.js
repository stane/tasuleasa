angular.module('starter.controllers', ['firebase'])

.controller('DashCtrl', function($scope, $http) {

        $scope.description = "Voluntarii nostri cred ca Tasuleasa Social este o tara in care le-ar placea sa traiasca. La noi, bunul-simt este regula de baza. Dar, in acelasi timp, credem in voluntariat, spirit de echipa, responsabilitate si solidaritate. Ne place sa credem ca plantam mai mult decat copaci, plantam idei. Viziunea noastra este ca, intr-o buna zi, toti cei care au trecut macar o data pe la Tasu sa schimbe Romania intr-o tara in care regulile nu sunt facute pentru a fi incalcate. Unii spun ca oamenii de la Tasuleasa pot muta muntii din loc.\nTot ceea ce am realizat este rodul incapatanarii cu care ne urmarim visul si al lucrurilor marunte de care nu incetam sa ne bucuram in fiecare zi: un rasarit de soare pe muntele Tasuleasa, un borcan cu dulceata de visine la care am muncit cu totii, zborul razant al unei berze negre sau o poveste la lumina unei torte suedeze cu un prieten drag.";
    })

.controller('EventsCtrl', function($scope, $http, Details, $cordovaBarcodeScanner, $cordovaDialogs, $state) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});
  $scope.events = Details.all().then(function(val){
      $scope.events = val;
      $scope.first = val[0];
  });

  $scope.remove = function(event) {
    Events.remove(event);
  };

  $scope.scanBarcode = function() {
        $cordovaBarcodeScanner.scan().then(function(imageData) {
            if(imageData.text !== undefined) {
                $http({
                    method: 'POST',
                    url: 'http://tasuleasa-social.appspot.com/event/' + imageData.text + '/volunteers',
                    params: {"volunteer": window.localStorage['userId']},
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function (data, status, headers, config) {
                    $cordovaDialogs.alert("Te-ai inscris cu succes la eveniment",
                        "Felicitari!", "ok").then(function () {
                            $state.go("tab.event-detail", {'eventId': imageData.text});
                        });
                }).error(function (data, status, headers, config) {
                    $cordovaDialogs.alert("A aparut o eroare! Te rugam sa reincerci",
                        "Eroare", "ok").then(function () {
                            $state.go("tab.event-detail", {'eventId': imageData.text});
                        });
                    console.log(data);
                    console.log(headers);
                    console.log(config);
                    console.log(status);
                });
            }
            console.log("Barcode Format -> " + imageData.format);
            console.log("Cancelled -> " + imageData.cancelled);
        }, function(error) {
            console.log("An error happened -> " + error);
        });
    };

})

.controller('EventDetailCtrl', function($scope, $stateParams, Details) {
  $scope.hideBackButton = !$scope.hideBackButton;

  $scope.detail = Details.get($stateParams.eventId);

  $scope.volunteers = Details.getVolunteersForCurrentEvent($stateParams.eventId).then(function(val){
  	$scope.volunteers = val;
  });
})

.controller('ProfileCtrl', function($scope, $stateParams, Details){
        $scope.userDetails = Details.accountDetails($stateParams.profileId).then(function(val){
            $scope.userDetails = val;
        });
        $scope.eventsForVolunteer = Details.getEventsForVolunteer($stateParams.profileId).then(function(val){
          $scope.eventsForVolunteer = val;
      });
    })

.controller('AccountCtrl', function($scope, $cordovaSms,  $cordovaDialogs, $stateParams, $firebaseAuth, $window, Details) {

        console.log("current userID = " + window.localStorage['userId']);
        toggleShow();

        $scope.userDetails = Details.accountDetails(window.localStorage['userId']).then(function(val){
        $scope.userDetails = val;
    });

    $scope.eventsForVolunteer = Details.getEventsForVolunteer(window.localStorage['userId']).then(function(val){
        $scope.eventsForVolunteer = val;
    });

  $scope.sendSms = function(){
  	console.log("send sms");
  	var options = {
    replaceLineBreaks: false // true to replace \n by a new line, false by default
 	};

  	$cordovaDialogs.confirm("Sunteti pe cale sa faceti o donatie", "Donatie", ['Doneaza', 'Cancel']).then(function(buttonIndex){
  		if(buttonIndex === 1){
	  		$cordovaSms
		      .send('0727783409', 'Donez pentru Tsuleasa', options)
		      .then(function() {
		        $cordovaDialogs.alert("Multumim pentru donatie!", "Donatie efectuata", "ok").then(function() {
      				// callback success
    			});
		      }, function(error) {
		        $cordovaDialogs.alert("Donatia nu a putut fi facuta", "Eroare", "ok").then(function() {
      				// callback success
    			});
		      });
  		}
  	});
  }

    $scope.login = function() {
        var ref = new Firebase('https://tasuleasa-fb-login.firebaseio.com');

        ref.authWithOAuthPopup("facebook", function(error, authData) {
            if (error) {
                console.log("Login Failed!", error);
            } else {
                console.log("Authenticated successfully with payload:", authData);

                console.log("userId", authData.facebook.id);
                console.log("name", authData.facebook.displayName);

                window.localStorage['userId'] = authData.facebook.id;
                window.localStorage['userName'] = authData.facebook.displayName;

                Details.join(authData.facebook.id, authData.facebook.displayName).then(function(profile) {
                    console.log(profile);
                    window.localStorage['userId'] = profile.Id;
                    window.localStorage['userName'] = profile.Name;
                    $window.location.reload(true);
                });
            }
        });
    }

    $scope.logout = function() {
        window.localStorage.clear();
        $window.location.reload(true);
    }

    $scope.toggleShow = function() {
        toggleShow();
    };

    function toggleShow() {
        if(window.localStorage['userId'] == undefined) {
            $scope.showLogging = true;
        } else {
            $scope.showLogging = false;
        }
    }

    })
