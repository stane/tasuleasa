angular.module('starter.services', ['ionic', 'ionicLazyLoad'])

.factory('Details', function($http, $q) {
      var events = [];

      var API_ENDPOINT = 'https://tasuleasa-social.appspot.com/';

      return {
        all: function() {
          return $http.get(API_ENDPOINT + 'events')
              .success(function(data, status, headers, config) {
              //console.log(status);
          })
              .error(function(data, status, headers, config) {
                console.log("error: " + status);
              })
              .then(function (response) {
                  events = response.data;
                return response.data;
              },
              function(response) {
                // something went wrong
                return $q.reject(response.data);
              });
    },
    remove: function(event) {
        events.splice(events.indexOf(chat), 1);
    },
    get: function(eventId) {
      for (var i = 0; i < events.length; i++) {
        if (events[i].Id === parseInt(eventId)) {
          return events[i];
        }
      }
      return null;
    },

    getVolunteersForCurrentEvent: function(eventId){
    	return $http.get(API_ENDPOINT + 'event/' + eventId + '/volunteers')
              .success(function(data, status, headers, config) {
              //console.log(status);
          })
              .error(function(data, status, headers, config) {
                console.log("error: " + status);
              })
              .then(function (response) {
                console.log(response.data);
                return response.data;
              },
              function(response) {
                // something went wrong
                return $q.reject(response.data);
              });
    },

    getFirstEvent: function(){
    	return events[0];
    },

    getEventsForVolunteer: function(profileId){
        return $http.get(API_ENDPOINT + 'volunteers/' + profileId + '/events')
            .success(function(data, status, headers, config) {
                console.log(API_ENDPOINT + 'volunteers/' + profileId + '/events');
            })
            .error(function(data, status, headers, config) {
                console.log("error: " + status);
            })
            .then(function (response) {
                console.log(response.data);
                return response.data;
            },
            function(response) {
                // something went wrong
                return $q.reject(response.data);
            });
    },

    accountDetails: function(profileId){
        console.log("profile ID = ", profileId);
        return $http.get(API_ENDPOINT + 'volunteers/' + profileId)
            .success(function(data, status, headers, config) {
                //console.log('http://tasuleasa-social.appspot.com/volunteers/' + profileId + '/events');
            })
            .error(function(data, status, headers, config) {
                console.log("error: " + status);
            })
            .then(function (response) {
                console.log(response.data);
                return response.data;
            },
            function(response) {
                // something went wrong
                return $q.reject(response.data);
            });
    },

    join: function(facebookId, name) {
        return $http.get(API_ENDPOINT + 'signin?user=' + facebookId + "&name=" + name)
            .success(function(data, status, headers, config) {
                //console.log('http://tasuleasa-social.appspot.com/volunteers/' + profileId + '/events');
            })
            .error(function(data, status, headers, config) {
                console.log("error: " + status);
            })
            .then(function (response) {
                console.log(response.data);
                return response.data;
            },
            function(response) {
                // something went wrong
                return $q.reject(response.data);
            });
    }
  };
});
